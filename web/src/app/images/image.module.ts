import { NgModule } from '@angular/core';
import { SharedModule } from '../shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImagesRoutingModule } from './image-routing.module';
import { ImageListComponent } from './image-list/image-list.component';
import { ImageViewComponent } from './image-view/image-view.component';

@NgModule({
  declarations: [
    ImageListComponent,
    ImageViewComponent
  ],
  imports: [
    SharedModule,
    ImagesRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ImageModule { }
