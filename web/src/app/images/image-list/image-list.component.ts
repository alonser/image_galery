import { Component, OnInit } from "@angular/core";

import { ImageService, Image } from "../image.service";
import { DomSanitizer } from "@angular/platform-browser";
@Component({
  selector: "app-image-list",
  templateUrl: "./image-list.component.html",
  styleUrls: ["./image-list.component.css"]
})
export class ImageListComponent implements OnInit {
  images: Image[] = [];

  constructor(
    private imageService: ImageService,
    private _sanitizer: DomSanitizer
  ) {}

  transformImage(img) {
    // return this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
    //              + img);
    return this._sanitizer.bypassSecurityTrustResourceUrl(img);
  }

  ngOnInit() {
    this.imageService
      .getAll()
      .subscribe((imageData: Image[]) => (this.images = imageData));
  }
}
