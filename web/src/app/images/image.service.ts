import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../environments/environment';
const server = environment.server;

export interface Image {
  _id: string;
  name: string;
  description: string;
  imageBase64: string;
}

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  constructor(private http: HttpClient) { }

  getAll(): Observable<Image[]> {
    return this.http.get<Image[]>(server + '/image');
  }
  findById(id: string): Observable<Image> {
    return this.http.get<Image>(server + '/image/findById?imageId=' + id);
  }
  update(image: Image): Observable<Image> {
    return this.http.patch<Image>(server + '/image/',image);
  }
  put(image: any): Observable<Image> {
    return this.http.put<Image>(server + '/image/',image);
  }
  delete(id: string): void {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: {
        id: id,
      },
    };
    this.http.delete(server + '/image/',options).subscribe((s) => {
      console.log(s);
    });
  }
}
