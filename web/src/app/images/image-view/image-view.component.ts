import { Component, ElementRef, HostListener, Input, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription, ReplaySubject, Observable, fromEvent } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageService, Image } from '../image.service';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, FormGroup, FormBuilder } from '@angular/forms';
import { pluck } from 'rxjs/operators';
import { Router } from '@angular/router';
@Component({
  selector: 'app-image-view',
  templateUrl: './image-view.component.html',
  styleUrls: ['./image-view.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: ImageViewComponent,
      multi: true
    }
  ],
})
export class ImageViewComponent implements OnInit, OnDestroy,ControlValueAccessor  {
  @Input() progress;
  onChange: Function;
  file: File | null = null;

  @HostListener('change', ['$event.target.files']) emitFiles( event: FileList ) {
    const file = event && event.item(0);
    //this.onChange(file);
    this.getOnChange(file);
    this.file = file;
  }
  image: Image = { name:"",_id:"",description:"",imageBase64:"" };
  private id: string;
  private subscription: Subscription;

  constructor(private route: ActivatedRoute, private imageService: ImageService,private _sanitizer: DomSanitizer,private host: ElementRef<HTMLInputElement>,private fb: FormBuilder,private router:Router) { }
  getOnChange(file){
    const fileReader = new FileReader();
      let imageToUpload = file;
      this.imageToBase64(fileReader, imageToUpload)
        .subscribe(base64image => {
          //console.log(base64image)
          this.image.imageBase64 = base64image;
          // do something with base64 image..
        });
  };

  imageToBase64(fileReader: FileReader, fileToRead: File): Observable<string> {
    fileReader.readAsDataURL(fileToRead);
    return fromEvent(fileReader, 'load').pipe(pluck('currentTarget', 'result'));
  }

  writeValue( value: null ) {
    // clear file input
    this.host.nativeElement.value = '';
    this.file = null;
  }

  registerOnChange( fn: Function ) {
    this.onChange = fn;
  }

  registerOnTouched( fn: Function ) {
  }

  messageCreate = "Add Image";
  angForm: FormGroup;
  createForm() {
    this.angForm = this.fb.group({
      name: [''],
      description: ['']
    });
  }
  saveImage(name, description) {
    this.image.description = description;
    this.image.name = name;
    if(this.image._id=="new-image"){
      let newImage = {
        description : description,
        name : name,
        imageBase64 : this.image.imageBase64
      }
      this.imageService.put(newImage).subscribe(
        (image: Image) => {
          console.log(image);
          this.image = image;
          this.router.navigate(['/image/']);
        }
      );
    }else{
      this.image["id"] = this.image._id;
    this.imageService.update(this.image).subscribe(
      (image: Image) => {
        console.log(image);
        this.image = image;
        this.router.navigate(['/image/']);
      }
    );
    }
  }


  deleteImage(){
    this.imageService.delete(this.image._id);
    this.router.navigate(['/image/']);
  }

  ngOnInit() {
    this.createForm();
    this.subscription = this.route.paramMap.subscribe(
      (map: ParamMap) => {
        this.id = map.get('id');
        if(this.id =="new-image"){
          this.messageCreate = "Create Image";
          this.image = { name:"",_id:"new-image",description:"",imageBase64:"" };
        }else{
          this.messageCreate = "Update Image";
          this.imageService.findById(this.id).subscribe(
            (image: Image) => {
              this.image = image;
              this.angForm.patchValue(image)
            }
          );
        }
       
       
      }
    );
  }
  transformImage(img){
    return this._sanitizer.bypassSecurityTrustResourceUrl(img);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }




}

