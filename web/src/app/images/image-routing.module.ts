import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImageListComponent } from './image-list/image-list.component';
import { ImageViewComponent } from './image-view/image-view.component';

const routes: Routes = [
  { path: ':id', component: ImageViewComponent },
  { path: '', component: ImageListComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ImagesRoutingModule { }
