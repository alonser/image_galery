//Require Mongoose
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

imageSchema = new Schema({
    name: String,
    location: String,
    description: String,
    imageBase64:String
});
var ImageModel = mongoose.model('Image', imageSchema);

module.exports = ImageModel;