const ImageModel = require("../models/image.model");
const AWS = require("aws-sdk");
const s3 = new AWS.S3();
class ImageController {
  static getImageById = async (req, res) => {
    let imageId = req.query.imageId;
    var getParams = {
      Bucket: "image-galery",
      Key: imageId
    };
    ImageModel.findById(imageId, async function(err, image) {
      if (err) return console.error(err);
      let promise = new Promise((resolve, reject) => {
        s3.getObject(getParams, function(err, data) {
          if (err) reject(err);
          let objectData = data.Body.toString("utf-8");
          resolve(objectData);
        });
      });
      image.imageBase64 = await promise;
      res.send(image);
    });
  };
  static getImage = async (req, res) => {
    ImageModel.find(async function(err, images) {
      for (const image of images) {
        var getParams = {
          Bucket: "image-galery",
          Key: image.id
        };
        let promise = new Promise((resolve, reject) => {
          s3.getObject(getParams, function(err, data) {
            if (err) reject(err);
            let objectData = data.Body.toString("utf-8");
            resolve(objectData);
          });
        });
        image.imageBase64 = await promise;
      }
      if (err) return console.error(err);
      res.send(images);
    });
  };
  static insertImage = async (req, res) => {
    let oNewImage = req.body;
    var nuevaImagen = new ImageModel(oNewImage);
    nuevaImagen.save(async function(err, nuevaImagen) {
      var nuevaImagen = nuevaImagen;
      if (err) return console.error(err);
      const params = {
        Bucket: "image-galery",
        Key: nuevaImagen.id, // File name you want to save as in S3
        Body: oNewImage.imageBase64
      };
      await s3.upload(params, function(err, data) {
        if (err) {
          throw err;
        }
        nuevaImagen.location = data.Location;

        nuevaImagen.save(function(err) {
          if (err) res.send(err);
          else res.send(nuevaImagen);
        });
      });
    });
  };
  static updateImage = async (req, res) => {
    var updateObject = req.body;
    ImageModel.findById(updateObject.id, async function(err, p) {
      var p = p;
      if (!p) res.send(new Error("Could not load Document"));
      else {
        const params = {
          Bucket: "image-galery",
          Key: updateObject.id,
          Body: updateObject.imageBase64
        };
        await s3.upload(params, function(err, data) {
          if (err) {
            throw err;
          }
          p.location = data.Location;
          p.name = updateObject.name;
          p.description = updateObject.description;
          p.key = updateObject.key;
          p.imageBase64 = updateObject.imageBase64;
          p.save(function(err) {
            if (err) res.send(err);
            else res.send(p);
          });
        });
      }
    });
  };
  static deleteImage = async (req, res) => {
    var deleObject = req.body;
    ImageModel.findById(deleObject.id, async function(err, p) {
      var p = p;
      if (!p) res.send(new Error("Could not load Document"));
      else {
        const params = {
          Bucket: "image-galery",
          Key: deleObject.id
        };
        await s3.deleteObject(params, function(err, data) {
          if (err) res.send(err);
          // error
          else {
            p.remove(function(err) {
              if (err) res.send(err);
              else res.send("Deleted");
            });
          } // deleted
        });
      }
    });
  };
}
module.exports = ImageController;
