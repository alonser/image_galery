const express = require('express');
const router = express.Router();
const ImageController = require("../controller/image.controller");

router.get("/", ImageController.getImage);
router.get("/findById", ImageController.getImageById);
router.put("/", ImageController.insertImage);
router.patch("/", ImageController.updateImage);
router.delete("/", ImageController.deleteImage);


module.exports = router;