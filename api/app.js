const express = require('express');
const routes = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const routesApp = require('./routes/routes');
require('./server/moongose.connect');
class App {
    app = express();
    specs;

    constructor() {
        this.config();
        this.app.listen(3000, () => console.log('Server running on port 3000'));   
    }

    config() {
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(cors());
        this.app.use(helmet());
        this.app.use("/api/v1", routesApp);
        // serving static files
        this.app.use(express.static("public"));
    }
}
module.exports = new App().app